from . import Command
from .. import utils
import json
import random
import discord
import urllib.parse
from urllib.request import Request, urlopen
import os


async def getImageSize(url):
    # This function gets the image size using urllib.request
    req = Request(url, headers={"User-Agent": "Mozilla/5.0"})
    path = urlopen(req)
    meta = path.info()
    return int(meta.get(name="Content-Length"))


class Recipe(Command):
    names = ["recipe", "food", "eat", "recipes", "cooking", "cook", "bake"]
    description = "Responds with a recipe from given query."
    usage = "!recipe [query]"
    examples = f"!recipe pasta"
    causes_spam = True

    async def execute_command(self, client, msg, content, **kwargs):
        api_key = client.config["recipe_api_key"]

        contributors = [
            "@ShirleyDan",
            "@A Warp Drive",
            "@A.Vulgare",
            "@smartboard",
            "@Phi11ipus",
            "@themanofcat",
            "@Cxider",
            "@Eigenvector",
            "@Shape Shifter",
        ]
        # Special thanks to @A.Vulgare, #A Warp Drive and @Phill1us for the tips
        footer_messages = [
            "Rubber Duck cooking tip #1: Don't try to catch a knife as it's falling",
            "Rubber Duck cooking tip #2: Use separate cutting boards for meat and vegetables",
            "Rubber Duck cooking tip #3: Remember to turn on the stove",
            "Rubber Duck cooking tip #4: Remember to turn off the stove",
            "Rubber Duck cooking tip #5: Flavor text doesn't actually taste good",
            "Rubber Duck cooking tip #6: Try some cheese and quackers",
            "Rubber Duck cooking tip #7: Don't leave dirty dishes for someone else",
            "Rubber Duck cooking tip #8: Contrary to popular belief, ducks ARE NOT food.",
            "Rubber Duck cooking tip #9: Invest in a baking scale.",
            "Rubber Duck cooking tip #10: Clean as you go.",
            "Rubber Duck cooking tip #11: Always taste your food before seasoning.",
            "Rubber Duck cooking tip #12: Don’t rinse pasta.",
            "Rubber Duck cooking tip #13: Honey is a natural preservative and will never spoil.",
            "Rubber Duck cooking tip #14: quack quack quack Quack \U0001F60B",
            "Rubber Duck cooking tip #15: When in doubt, throw it out",
            "Rubber Duck cooking tip #16: Compost.",
            "Rubber Duck cooking tip #17: Don't make mint bars",
            "Rubber Duck cooking tip #18: Clean out your fridge, freezer, pantry, and spice rack regularly.",
            f"Rubber Duck cooking tip #69: If you need more tips, ask {random.choice(contributors)}",
        ]

        if len(content) == 0 or content == "random":
            return await utils.delay_send(msg.channel, "No random recipes... yet!")
        elif content == "spoonacular":
            return await utils.delay_send(
                msg.channel, "Spoonacular API: https://spoonacular.com/food-api"
            )

        urllib.parse.quote(content)

        # Easter egg
        if "rust" in content:
            query = f"https://api.spoonacular.com/recipes/complexSearch?apiKey={api_key}&query=crab&addRecipeInformation=true&number=10"
        else:
            query = f"https://api.spoonacular.com/recipes/complexSearch?apiKey={api_key}&query={content}&addRecipeInformation=true&number=10"
        async with utils.get_aiohttp().get(query) as recipe_request:
            if recipe_request.status != 200:
                return await utils.delay_send(
                    msg.channel, "Having trouble getting this recipe, please try again!"
                )

            recipes = json.loads(await recipe_request.read())["results"]

            if len(recipes) == 0:
                return await utils.delay_send(
                    msg.channel, "No recipe found for that query."
                )
            # get a random recipe out of 10 recipes
            random_recipe = random.choice(recipes)
            title = random_recipe["title"]
            image = random_recipe["image"]
            # Get recipe link(Reason Im not using the spoonacularSourceUrl
            # here is to give credit to the original author of the recipe)
            link = random_recipe["sourceUrl"]

            response = discord.Embed(title=title, url=link)
            footer_message = random.choice(footer_messages)
            if "duck" in content:
                response.set_footer(text=footer_messages[7])
            else:
                response.set_footer(text=footer_message)

            # Fix two spoonacular API bugs
            if "spoonacular" in link:
                # 1) When spoonacular returns a recipe from their site the sourceUrl paramater
                # directs to a broken link, using the spoonacularSourceUrl you can get a working link
                link = random_recipe["spoonacularSourceUrl"]
                # 2) Recipes with no pictures in the thumbnail, get the size, compare with
                # placeholder image sizes and do not display an image
                size = await getImageSize(image)
                if size in (3668, 6892):
                    return await msg.channel.send(embed=response)

            if len(image) != 0:
                response.set_image(url=image)
            return await msg.channel.send(embed=response)
